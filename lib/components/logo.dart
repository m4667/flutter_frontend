import 'package:flutter/material.dart';

class AppBarLogo extends StatelessWidget {
  const AppBarLogo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 10), // Hacky solution to align logo
        Image.asset("assets/Geoverse_logo_black_large.png",
            fit: BoxFit.fitHeight),
      ],
    );
  }
}
