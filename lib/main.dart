// Copyright 2013 The Flutter Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// ignore_for_file: public_member_api_docs

import 'dart:async';
import 'dart:convert' show json;
import 'components/logo.dart';
import "package:http/http.dart" as http;
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(
  // Optional clientId
  // clientId: '479882132969-9i9aqik3jfjd7qhci1nqf0bm2g71rm1u.apps.googleusercontent.com',
  scopes: <String>[
    'email',
    'https://www.googleapis.com/auth/contacts.readonly',
  ],
);

void main() {
  runApp(
    MaterialApp(
      title: 'Geoverse',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    ),
  );
}

// class SignInDemo extends StatefulWidget {
//   @override
//   State createState() => SignInDemoState();
// }

// class SignInDemoState extends State<SignInDemo> {
//   GoogleSignInAccount? _currentUser;
//   String _contactText = '';

//   @override
//   void initState() {
//     super.initState();
//     _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount? account) {
//       setState(() {
//         _currentUser = account;
//       });
//       if (_currentUser != null) {
//         _handleGetContact(_currentUser!);
//       }
//     });
//     _googleSignIn.signInSilently();
//   }

// Future<void> _handleGetContact(GoogleSignInAccount user) async {
//   setState(() {
//     _contactText = "Loading contact info...";
//   });
//   final http.Response response = await http.get(
//     Uri.parse('https://people.googleapis.com/v1/people/me/connections'
//         '?requestMask.includeField=person.names'),
//     headers: await user.authHeaders,
//   );
//   if (response.statusCode != 200) {
//     setState(() {
//       _contactText = "People API gave a ${response.statusCode} "
//           "response. Check logs for details.";
//     });
//     print('People API ${response.statusCode} response: ${response.body}');
//     return;
//   }
//   final Map<String, dynamic> data = json.decode(response.body);
//   final String? namedContact = _pickFirstNamedContact(data);
//   setState(() {
//     if (namedContact != null) {
//       _contactText = "I see you know $namedContact!";
//     } else {
//       _contactText = "No contacts to display.";
//     }
//   });
// }

// String? _pickFirstNamedContact(Map<String, dynamic> data) {
//   final List<dynamic>? connections = data['connections'];
//   final Map<String, dynamic>? contact = connections?.firstWhere(
//     (dynamic contact) => contact['names'] != null,
//     orElse: () => null,
//   );
//   if (contact != null) {
//     final Map<String, dynamic>? name = contact['names'].firstWhere(
//       (dynamic name) => name['displayName'] != null,
//       orElse: () => null,
//     );
//     if (name != null) {
//       return name['displayName'];
//     }
//   }
//   return null;
// }

// Future<void> _handleSignIn() async {
//   try {
//     await _googleSignIn.signIn();
//   } catch (error) {
//     print(error);
//   }
// }

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  GoogleSignInAccount? _currentUser;
  @override
  void initState() {
    super.initState();
    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount? account) {
      setState(() {
        _currentUser = account;
      });
      if (_currentUser != null) {
        // _handleGetContact(_currentUser!);
      }
    });
    _googleSignIn.signInSilently();
  }

  Future<void> _handleSignIn() async {
    try {
      await _googleSignIn.signIn();
    } catch (error) {
      print(error);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const AppBarLogo(),
      ),
      body: Center(
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset("assets/Landing.jpg", fit: BoxFit.fitHeight),
            const Spacer(),
            const Center(
              child:
                  Text("Tap below to log in", style: TextStyle(fontSize: 20)),
            ),
            const SizedBox(height: 10),
            Column(
              children: <Widget>[
                Center(
                    child: Container(
                        padding: const EdgeInsets.all(30.0),
                        width: double.infinity,
                        height: 200,
                        decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10)),
                          color: Colors.blueGrey[100],
                        ),
                        child: SizedBox(
                            width: 100,
                            child: TextButton(
                              child: const Text("Enter the Geoverse",
                                  style: TextStyle(
                                    fontSize: 30,
                                    fontWeight: FontWeight.bold,
                                  )),
                              onPressed: () {
                                _handleSignIn();
                              },
                              style: ButtonStyle(
                                foregroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Colors.black),
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Colors.blueGrey),
                              ),
                            )))),
              ],
            )
          ],
        ),
      ),
    );
  }
}
